#behandeln
DROP TABLE behandeln;

#gehoert
DROP TABLE gehoert;

#notify
DROP TABLE notify;

#Kommentar
DROP TABLE Kommentar;

#Bild
DROP TABLE Bild;

#Aufenthalt
DROP TABLE Aufenthalt;

#ICD10
DROP TABLE ICD10;

#Login
DROP TABLE Login;

#Person
DROP TABLE Person;