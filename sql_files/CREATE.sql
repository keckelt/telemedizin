#erstellt Schema wenn nicht vorhanden und wechselt auf dieses
CREATE SCHEMA IF NOT EXISTS gaba;
USE gaba;
SET CHARACTER SET 'utf8';

#Person
CREATE TABLE Person
(	
	PID INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	Titel VARCHAR(30),
	Vorname VARCHAR(30) NOT NULL,
	Nachname VARCHAR(30) NOT NULL,
	Geburtsdatum DATE NOT NULL,
	SVNR VARCHAR(15) NOT NULL,
	Tel VARCHAR(15),
	Email VARCHAR(50),
	Geschlecht VARCHAR(1) NOT NULL CHECK(Typ IN ('m','w')),
	isArzt BOOLEAN NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#Login
CREATE TABLE Login
(
	Benutzername VARCHAR(30) PRIMARY KEY NOT NULL,
	Passwort VARCHAR(30) NOT NULL,
	Person_id INTEGER NOT NULL, 
	FOREIGN KEY (Person_id)
	REFERENCES Person(PID),
	lastAction DATETIME
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#ICD10
CREATE TABLE ICD10 (
  Code VARCHAR(5) PRIMARY KEY NOT NULL,
  Beschreibung VARCHAR(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#Aufenthalt
CREATE TABLE Aufenthalt
(
	AID INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	Diagnosenbeschreibung VARCHAR(250),
	Person_id INTEGER NOT NULL,
	von DATE,
	bis DATE CHECK (bis >= von),
	FOREIGN KEY (Person_id)
	REFERENCES Person(PID),
	icd_code VARCHAR(5),
	FOREIGN KEY (icd_code)
	REFERENCES ICD10(Code)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#Bild
#DATETIME: 'YYYY-MM-DD HH:MM:SS[.fraction]', bsp '1000-01-01 00:00:00.000000'
CREATE TABLE Bild
(
	BID INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	Datum DATETIME NOT NULL,
	Speicherort VARCHAR(250) NOT NULL,
	Beschreibung VARCHAR(150),
	IsOriginal BOOLEAN NOT NULL,
	InUse BOOLEAN NOT NULL,
	URL VARCHAR(150),
	Person_id INTEGER NOT NULL,
	FOREIGN KEY (Person_id)
	REFERENCES Person(PID),

	teil_von INTEGER,
	FOREIGN KEY (teil_von)
	REFERENCES Bild(BID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#Kommentar
CREATE TABLE Kommentar
(
	KID INTEGER AUTO_INCREMENT PRIMARY KEY NOT NULL,
	Inhalt VARCHAR(150) NOT NULL,
	Datum DATETIME NOT NULL,
	x INTEGER NOT NULL,
	y INTEGER NOT NULL,
	Bild_id INTEGER NOT NULL,
	FOREIGN KEY (Bild_id)
	REFERENCES Bild(BID),

	Arzt_id INTEGER NOT NULL,
	FOREIGN KEY (Arzt_id)
	REFERENCES Person(PID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#behandeln
CREATE TABLE behandeln
(
	Aufenthalt_id INTEGER NOT NULL,
	Arzt_id INTEGER NOT NULL,
	IsBehandler BOOLEAN,
	PRIMARY KEY(Aufenthalt_id,Arzt_id),

	FOREIGN KEY arzt_fk(Arzt_id)
	REFERENCES Person(PID),

	FOREIGN KEY aufenthalt_fk(Aufenthalt_id)
	REFERENCES Aufenthalt(AID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#gehoert
CREATE TABLE gehoert
(
	Aufenthalt_id INTEGER NOT NULL,
	Bild_id INTEGER NOT NULL,
	PRIMARY KEY(Aufenthalt_id,Bild_id),

	FOREIGN KEY aufenthalt_fk(Aufenthalt_id)
	REFERENCES Aufenthalt(AID),

	FOREIGN KEY bild_fk(Bild_id)
	REFERENCES Bild(BID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#notify
CREATE TABLE notify
(
	Kommentar_id INTEGER NOT NULL,
	Arzt_id INTEGER NOT NULL,
	PRIMARY KEY(Kommentar_id,Arzt_id),

	FOREIGN KEY arzt_fk(Arzt_id)
	REFERENCES Person(PID),

	FOREIGN KEY kommentar_fk(Kommentar_id)
	REFERENCES Kommentar(KID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
