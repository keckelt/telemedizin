<!-- Überprüfe ob session vorhanden ist, sonst Login-Aufforderung -->
<?php include('session.php'); ?>
<!-- IF POST is set, write new shares to database -->
<?php
    if (isset($_POST['submit']))
	{
        $aufenthaltID = $_POST['aufenthaltid'];
        if(!isset( $_POST['isDiagnose']))
        {
			if(isset($_POST['isEnde'])) {
				//Auftrag beenden
			    $conUpdateEnd = new mysqli("localhost", "root", "", "gaba");
				if ($conUpdateEnd->connect_error) {
					die("Connection failed: " . $conUpdateEnd->connect_error);
				}
				$sqlUpdate = "UPDATE Aufenthalt SET bis = NOW() WHERE AID = $aufenthaltID;";
				
				if (!$conUpdateEnd->query($sqlUpdate)) {
					echo "Error: " . $sqlUpdate . "<br>" . $conUpdateEnd->error;
				}
				$conUpdateEnd->close();
			} else {
				//echo "Form was submitted: aid = $aufenthaltID";
				/*Alles aus der Tabelle behandeln mit ggb. AID löschen, außer den isBehandler=true Arzt */
				$conUpdateShares = new mysqli("localhost", "root", "", "gaba");
				if ($conUpdateShares->connect_error) {
					die("Connection failed: " . $conUpdateShares->connect_error);
				}
				$sqlDelete = "DELETE FROM behandeln WHERE Aufenthalt_id = $aufenthaltID AND IsBehandler = 0;";
				/* alle gesetzten checkboxen in die tabelle schreiben */
				if ($conUpdateShares->query($sqlDelete) === TRUE) {
					if (!empty($_POST['checkList'])) {
						//Create Comment for each Picture
						// --> Get each picture
						$getBilder = "SELECT Bild_id FROM gehoert WHERE Aufenthalt_id=$aufenthaltID;";
						$result = $conUpdateShares->query($getBilder);
						$commentIDs = array();

						if ($result->num_rows > 0) {
							// output data of each row
							while($row = $result->fetch_assoc()) {
								//Create Comment
								$now = date("Y-m-d H:i:s");
								$insert = "INSERT INTO `gaba`.`Kommentar` (`Inhalt`, `Datum`, `x`, `y`, `Bild_id`, `Arzt_id`) VALUES ('Einladung', '$now','-1', '-1', '".$row["Bild_id"]."', '".$_SESSION['pid']."');";

								if ($conUpdateShares->query($insert) === TRUE) {
									//echo "insert comment succesful";
								   $commentIDs[] = mysqli_insert_id($conUpdateShares);
								} else {
									echo "Error inserting comment: " . $conUpdateShares->error;
								}
							}
						}

						//Freigabe für jeden Arzt Speichern
						foreach($_POST['checkList'] as $docID) {
							$sqlInsert = "INSERT INTO behandeln (Arzt_id, Aufenthalt_id, IsBehandler) VALUES ($docID, $aufenthaltID, 0);";
							if ($conUpdateShares->query($sqlInsert) === TRUE) {
								//echo "New record created successfully";

								//Add Notification for each doctor:
								if(count(commentIDs)>0)
								{
									foreach($commentIDs as $commentID){
										$insert = "INSERT INTO `gaba`.`notify` (`Kommentar_id`, `Arzt_id`) VALUES ('$commentID', '". $docID ."')";
										 if ($conUpdateShares->query($insert) === TRUE) {
											//echo "insert notification succesful";
										} else {
											echo "Error inserting notification: " . $conUpdateShares->error ."<br>".$insert;
										}
									}
								}
							} else {
								echo "Error: " . $sqlInsert . "<br>" . $conUpdateShares->error;
							}
						}
					}
				} else {
					echo "Error deleting records: " . $conUpdateShares->error;
				}
				$conUpdateShares->close();
			}
        }else
        {				
			if($_POST['diagnose'])
			{
				$diagnose = $_POST['diagnose'];
				$anmerkung = $_POST['anmerkung'];
				
				$splitstr = explode(": ", $diagnose);						
				$icd10_code = $splitstr[0]; 
			
			    $conUpdateDiagnose = new mysqli("localhost", "root", "", "gaba");
				if ($conUpdateDiagnose->connect_error) {
					die("Connection failed: " . $conUpdateDiagnose->connect_error);
				}
				$sqlUpdate = "UPDATE Aufenthalt SET Diagnosenbeschreibung = '$anmerkung', icd_code = '$icd10_code' WHERE AID = $aufenthaltID;";
				if (!$conUpdateDiagnose->query($sqlUpdate)) {
					echo "Error: " . $sqlUpdate . "<br>" . $conUpdateDiagnose->error;
				}

				$conUpdateDiagnose->close();	
			}
        }
	}
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Description: http://ningbit.github.io/blog/2013/09/30/html5-boilerplate-explained-in-simple-terms/ -->
<html>
    <head>
	<!-- Meta Information -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="title" content="GABA Portal">
        <meta name="description" content="Online Portal zur gemeinsamen Betrachtung und Diagnose von DICOM Bildern.">
        <meta name="robots" content="index,nofollow">

        <!-- Not good here: <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <!-- Page Title -->
        <title>GABA Portal</title>

    <!-- FavIcons -->
    <!-- Apple -->
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
        <meta name="apple-mobile-web-app-title" content="GABA">
    <!-- Browsers -->
        <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <!-- Windows 8 -->
        <meta name="msapplication-TileColor" content="#603cba">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="application-name" content="GABA">
    <!-- Style Sheets -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/component.css">
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <!-- Load Fonts--> 
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto'  type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- JavaScript -->
    <!-- Note: Avoid loading javascript here: load it at the end of the page. see yahoo performance guide-->
    <!-- But: Load modernizr here to support browsers who don't know HTML5 tags -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>

    <body>
        <div id="pageBackground">
            <?php
                include("header_portal.php");
                include("nav.php")
            ?>
            <!-- specific page content -->
            <?php
                $pid = $_GET['pid'];
				$arztid = $_SESSION['pid'];
				$curURL = $_SERVER['PHP_SELF']."?pid=".$pid;

                $verbindung = mysql_connect("localhost", "root", "")
                    or die("Verbindung zur Datenbank konnte nicht hergestellt werden");
                    mysql_select_db("gaba") or die ("Datenbank konnte nicht ausgewählt werden");
				
				mysql_query("SET NAMES 'utf8'");
				mysql_query("SET CHARACTER SET 'utf8'");

                $abfrage = "SELECT Nachname,Vorname,SVNR,Geburtsdatum,Tel,Email,Geschlecht FROM gaba.Person WHERE pid=$pid;";

                $ergebnis = mysql_query($abfrage);

                $row = mysql_fetch_object($ergebnis);
                if($row)
                {
                    print "<h2>$row->Nachname, $row->Vorname</h2>";
                    echo "<div class=\"person-data\">";
                     echo "<dl class=\"clearfix\">";
                    foreach ($row as $key => $value) {
                          print "<p><dt class=\"label\">$key:</dt><dd>$value</dd></p>";
                    }
                    echo "</dl>";
                    echo "</div>";
                }
                echo "<h2 class=\"headlineButton clearfix\" >Aufenthalte";

                //button um neuen aufenthalt anzulegen
                echo "<form id=\"newStay\" action=\"$curURL\" method=\"post\">";
                echo "<input type=\"hidden\" name=\"person_id\" value=\"$pid\">";
                echo "<input type=\"hidden\" name=\"arzt_id\" value=\"$arztid\">";
                echo "<input name=\"addStay\"  class=\"button\" id=\"newStayBtn\" type=\"submit\" value=\"Neuer Aufenthalt\">";
                echo "</form></h2>";

                if ( isset($_POST['addStay']))
                {
                    $pid = $_POST["person_id"];
                    $arztid = $_POST["arzt_id"];
                    // Create connection
                    $conn = mysqli_connect("localhost","root","", "gaba");

                    // Check connection
                    if (mysqli_connect_errno()) {
                        printf("Connect failed: %s\n", mysqli_connect_error());
                        exit();
                    }

                    $sql = "SELECT COUNT(*) AS ANZ FROM Aufenthalt WHERE Person_id=$pid AND bis IS NULL";
                    if($result = mysqli_query($conn,$sql))
                    {
                        while ($row = mysqli_fetch_array($result, MYSQLI_NUM)) 
                        {
                            $anz = $row[0];
                        }
                    }

                    if($anz==0)
                    {
                        $now = date("Y-m-d H:i:s"); 
                        $sql = "INSERT INTO Aufenthalt(Diagnosenbeschreibung,Person_id,von,bis) VALUES(NULL,$pid,'$now',NULL)";

                        if (mysqli_query($conn, $sql)) 
                        {
                            // -------- get new aid -----------------------------------------
                            $new_aid = mysqli_insert_id($conn);
                            //echo "New record created successfully. Last inserted ID is: " . $new_aid;
                        } else {
                            //echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                        }

                        //$arztid = $_SESSION['pid'];
                        $sql_beh = "INSERT INTO behandeln(Aufenthalt_id, Arzt_id, IsBehandler) VALUES($new_aid,$arztid,true)";

                        if (mysqli_query($conn, $sql_beh)) {
                              //  echo "New record created successfully";
                        } else {
                            //echo "Error: " . $sql_gehoert . "<br>" . mysqli_error($conn);
                        }

                    }
                    //close connection
                    mysqli_close($conn);

                }

                $abfrage = "SELECT AufenthaltArzt.*,Person.Titel,Person.Vorname,Person.Nachname, ICD10.Beschreibung FROM (SELECT Aufenthalt.*,behandeln.Arzt_id,behandeln.IsBehandler FROM Aufenthalt JOIN behandeln ON Aufenthalt.AID=behandeln.Aufenthalt_id WHERE isBehandler=true AND Person_id=$pid) as AufenthaltArzt JOIN Person ON AufenthaltArzt.Arzt_id=Person.PID LEFT JOIN ICD10 ON icd_code=Code ORDER BY AID desc;";
                $ergebnis = mysql_query($abfrage);
				
                echo "<div id=\"accordion\">";
                while ($row = mysql_fetch_object($ergebnis)) {
                    //Aufenthaltsdaten:
                    if($row->bis!=NULL)
                    {
                        echo "<h3>Aufenthalt #$row->AID <span class=\"date\" style=\"font-size:medium\"><span class=\"show\">von</span> $row->von <span class=\"show\">bis</span> $row->bis</span></h3>";
                    }else
                    {
                        echo "<h3>Aufenthalt #$row->AID <span class=\"date\" style=\"font-size:medium\"><span class=\"show\">von</span> $row->von</span></h3>";
                    }


                    echo "<div class=\"clearfix\">";
					echo "<div style=\"float:left;width:50%;\">";
                    echo "<p id=\"diagnose\"><span class=\"label\">ICD-Diagnose: </span> ";
                    if($row->icd_code)
                    {
                        echo $row->icd_code;
                    }else
                    {
                        echo " Keine. ";
                    }
					
					//Anzeige der Diagnosenbearbeitung nur wenn Arzt der behandelnde Arzt ist
					if($row->Arzt_id == $arztid && !isset($row->bis))
					{
						$name = "openModalDiagnose$row->AID";
						echo "<a href=\"#$name\" title=\"Diagnose bearbeiten\" style=\"color:white\">";
						echo "     <span class=\"fa-stack fa-lg\">";
						echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
						echo "         <i class=\"fa fa-pencil fa-stack-1x \"></i>";
						echo "     </span>";
						echo "</a>";

						createDialogBox($name,$row->AID,$curURL,$row->icd_code,$row->Diagnosenbeschreibung);
					}
                    echo "</p>";
					
					if($row->Beschreibung) {
						echo "<p id=\"icdbesch\"><span class=\"label\">Beschreibung: </span> ";
						echo $row->Beschreibung;
					    echo "</p>";
					}
					
					if($row->Diagnosenbeschreibung)
					{
						echo "<p id=\"diagnoseanmerkung\"><span class=\"label\">Anmerkung: </span> ";
                        echo $row->Diagnosenbeschreibung . " ";
						echo "</p>";
                    }					
                    
                    //Aufenthaltsbilder:
                    $bilderQuery = "SELECT * FROM gaba.gehoert JOIN Bild on gehoert.Bild_id=Bild.BID where Aufenthalt_id=$row->AID AND isOriginal=1";
                     $bilder = mysql_query($bilderQuery);
                    while ($bild = mysql_fetch_object($bilder)) {
                        echo "<a href=\"bild.php?bid=$bild->BID&aid=$row->AID\"><img class=\"thumbnail\" src='$bild->Speicherort' alt=\"$bild->Beschreibung\"/></a>";
                    }
					
					if($row->Arzt_id == $arztid && !isset($row->bis))
                    {
                        echo "<form action=\"upload.php\" method=\"post\" enctype=\"multipart/form-data\">";
                        echo "<input type=\"hidden\" name=\"aufenthalt\" value=\"$row->AID\"></input>";
                        echo "<input type=\"hidden\" name=\"patient\" value=\"".$_GET['pid']."\"></input>";
                        echo "<input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\">";
                        echo "<input type=\"submit\" class=\"button\" value=\"Upload Image\" name=\"submit\">";
                        echo "</form>";
                    }
					
					echo "</div>";
					//Freigabebox
					echo "<div style=\"float:left;width:50%;\">";
					echo "<p><span class=\"label\">Behandelnder Arzt:</span> <a href=\"patient.php?pid=$row->Arzt_id\">$row->Titel $row->Vorname $row->Nachname</a></p>";
					
					echo "<p><span class=\"label\">Freigegeben für:</span> ";
					//Bearbeitungsstift
					if($row->Arzt_id == $arztid && !isset($row->bis)) {
						$name = "openModal$row->AID";
						echo "<a href=\"#$name\" title=\"Freigaben bearbeiten\">";
						echo "     <span class=\"fa-stack fa-lg\">";
						echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
						echo "         <i class=\"fa fa-pencil fa-stack-1x \"></i>";
						echo "     </span>";
						echo "</a>";
					}
					//Anzeigen der aktuellen Freigaben (alle Einträge aus Tabelle behandeln mit aktuellem aufenthalt, außer aktuell eingeloggter user)
					$link = mysqli_connect("localhost", "root", "", "gaba");
                    if ($shared_stmt = mysqli_prepare($link, "SELECT P.* FROM (SELECT * FROM behandeln WHERE Aufenthalt_id=?) A JOIN Person P ON (A.Arzt_id=P.PID) where A.Arzt_id <> ? order by P.Nachname ASC"))
					{
                        mysqli_stmt_bind_param($shared_stmt, "ii", $row->AID, $_SESSION['pid']);
                        mysqli_stmt_execute($shared_stmt);
                        $shared_ergebnis = mysqli_stmt_get_result($shared_stmt);

						echo "<ul class=\"cursharelist\">";
                        while ($shared_row = mysqli_fetch_array($shared_ergebnis, MYSQLI_NUM))
                        {
                            echo "<li>";
                            printf("<a href=\"patient.php?pid=%s\">%s, %s</a>", $shared_row[0], $shared_row[3], $shared_row[2], $shared_row[4]);
							$chkOnline = "SELECT * FROM `Login` WHERE  lastAction >  DATE_SUB(NOW(), INTERVAL 10 MINUTE) and Person_id=$shared_row[0];";
							if(mysql_num_rows(mysql_query($chkOnline))==0)
							{
								echo "<div class=\"circle red\"></div>";
							}else
							{
								echo "<div class=\"circle green\"></div>";
							}
                            echo "</li>";
                        }
                        echo "</ul>";

                        mysqli_stmt_close($shared_stmt);
                    }
					
					//Anzeige der Freigabeverwaltung wenn aktueller Arzt behandelnder Arzt (isBehandler = true)
                    
                    /*if($row->Arzt_id == $arztid && !isset($row->bis))
                    {
                        echo "<form action=\"upload.php\" method=\"post\" enctype=\"multipart/form-data\">";
                        echo "<input type=\"hidden\" name=\"aufenthalt\" value=\"$row->AID\"></input>";
                        echo "<input type=\"hidden\" name=\"patient\" value=\"".$_GET['pid']."\"></input>";
                        echo "<input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\">";
                        echo "<input type=\"submit\" class=\"button\" value=\"Upload Image\" name=\"submit\">";
                        echo "</form>";
                    }*/
                    echo "</div>";
					if($row->Arzt_id == $arztid && !isset($row->bis))
					{
						//echo "<br /><a href=\"#openModal$row->AID\" class=\"button\" style=\"color:white;float:left;\">Freigaben verwalten</a>";
						echo "<a href=\"#openModalEnd$row->AID\" class=\"button\" style=\"color:white;float:right;\">Aufenthalt beenden</a>";
					}
					//echo "</div>";
					/* Aufenthalt beenden ModalDialog */
					echo "<div id=\"openModalEnd$row->AID\" class=\"modalDialog\">";
						echo "<div>";
							echo "<a href=\"#close\" title=\"Schließen\" class=\"close\">X</a>";
							echo "<h2>Aufenthalt abschließen?</h2>";
							echo "<form id=\"shareForm\" method=\"POST\" action=\"$curURL\">";
								echo "<input type=\"hidden\" name=\"aufenthaltid\" value=\"$row->AID\">";
								echo "<input type=\"hidden\" name=\"isEnde\" value=\"true\">";
								echo "<div style=\"text-align: center\">";
									echo "<input class=\"button\" id=\"submit\" name=\"submit\" type=\"submit\" value=\"Abschließen!\" >";
								echo "</div>";
							echo "</form>";
						echo "</div>";
					echo "</div>";
					/* ENDE - ENDE MODAL */
					/* FREIGABE MODAL BOX 1/Accordion*/
					echo "<div id=\"openModal$row->AID\" class=\"modalDialog\">";
						echo "<div>";
							echo "<a href=\"#close\" title=\"Schließen\" class=\"close\">X</a>";
							echo "<h2>Freigaben verwalten</h2>";
							echo "<form id=\"shareForm\" method=\"POST\" action=\"$curURL\">";
									echo "<input type=\"hidden\" name=\"aufenthaltid\" value=\"$row->AID\">";

									$verbindung = mysql_connect("localhost", "root", "")
									or die("Verbindung zur Datenbank konnte nicht hergestellt werden");
									mysql_select_db("gaba") or die ("Datenbank konnte nicht ausgewählt werden");

									$abfrage = "SELECT P.PID,P.Nachname,P.Vorname,B.Aufenthalt_id FROM gaba.Person P left join gaba.behandeln B on (P.PID = B.Arzt_id and B.Aufenthalt_id = $row->AID) WHERE P.isArzt=true AND P.PID <> $arztid order by B.Aufenthalt_id DESC, P.Nachname ASC;";

									$ergebnisDocs = mysql_query($abfrage);

									echo "<ul class=\"personlist sharelist onlinelist\">";
									while ($rowDocs = mysql_fetch_array($ergebnisDocs, MYSQL_NUM)) {
										echo "<li class=\"clearfix\">";
                                        $chkOnline = "SELECT * FROM `Login` WHERE  lastAction >  DATE_SUB(NOW(), INTERVAL 10 MINUTE) and Person_id=$rowDocs[0];";
                                        if(mysql_num_rows(mysql_query($chkOnline))==0)
                                        {
                                            echo "<div class=\"circle red\"></div>";
                                        }else
                                        {
                                            echo "<div class=\"circle green\"></div>";
                                        }

										if(isset($rowDocs[3])) {
											printf("<input name=\"checkList[]\" type=\"checkbox\" value=\"%s\" checked style=\"float:right\"> <a href=\"patient.php?pid=%s\">%s, %s </a> </input>", $rowDocs[0], $rowDocs[0], $rowDocs[1], $rowDocs[2]);
										} else {
											printf("<input name=\"checkList[]\" type=\"checkbox\" value=\"%s\" style=\"float:right\"> <a href=\"patient.php?pid=%s\">%s, %s </a> </input>", $rowDocs[0], $rowDocs[0], $rowDocs[1], $rowDocs[2]);
										}

										echo "</li>";
									}
									echo "</ul>";
								echo "<div style=\"text-align: right\">";
									echo "<input class=\"button\" id=\"submit\" name=\"submit\" type=\"submit\" value=\"Bestätigen\" >";
								echo "</div>";
							echo "</form>";
						echo "</div>";
					echo "</div>";
					/* ENDE - FREIGABE MODAL BOX 1/Accordion*/
                    echo "</div>";
                }
                mysql_close($verbindung);
                echo "</div>";
            ?>

        </div>
            <!-- specific page content end -->
        <?php include("footer.html"); ?>
        <!-- JavaScript -->
        <!-- Note: Place as much of your scripts as possible here. Because: DOM is ready now, i.e. not timing issues -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Load local copy if Google CDN is down / unreachable -->
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script>
            $( "#accordion" ).accordion();
        </script>
        <script src="https://togetherjs.com/togetherjs-min.js"></script>
        <script src="js/plugins.js"></script>
		<script type="text/javascript" src="js/scriptICD.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>

<?php

 function createDialogBox($name,$aid,$curURL,$icd,$anmerkung) {

     echo "<div id=\"$name\" class=\"modalDialog\">";
        echo "<div>";
            echo "<a href=\"#close\" title=\"Schließen\" class=\"close\">X</a>";
            echo "<h2>Diagnose stellen</h2>";
            echo "<form id=\"diagnoseForm\" method=\"POST\" action=\"$curURL\">";
                echo "<input type=\"hidden\" name=\"aufenthaltid\" value=\"$aid\">";
                echo "<input type=\"hidden\" name=\"isDiagnose\" value=\"true\">";
				echo "<div class=\"icd_container\">";
					if($icd) 
					{
						echo "<input type=\"text\" name=\"diagnose\" value=$icd id=\"icdcode\" onkeyup=\"autocomplet()\" autocomplete=\"off\" placeholder=\"nach ICD10 suchen\">";
					} else {
						echo "<input type=\"text\" name=\"diagnose\" id=\"icdcode\" onkeyup=\"autocomplet()\" autocomplete=\"off\" placeholder=\"nach ICD10 suchen\">";					
					}
					echo "<ul id=\"listICD\"></ul>";
				echo "</div>";	
				echo "<br />";
				echo "<br />";
				echo "Anmerkungen";
				if($anmerkung) {
					echo "<textarea name=\"anmerkung\" cols=\"39\" rows=\"5\">$anmerkung</textarea>";
				} else {
					echo "<textarea name=\"anmerkung\" cols=\"39\" rows=\"5\"></textarea>";
				}
				echo "<br />";
				echo "<br />";
                echo "<div style=\"text-align: right\">";
                    echo "<input class=\"button\" id=\"submit\" name=\"submit\" type=\"submit\" value=\"Bestätigen\" >";
                echo "</div>";
            echo "</form>";
        echo "</div>";
    echo "</div>";
 }

?>