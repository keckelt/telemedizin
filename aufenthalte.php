<!-- Überprüfe ob session vorhanden ist, sonst Login-Aufforderung -->
<?php include('session.php'); ?>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Description: http://ningbit.github.io/blog/2013/09/30/html5-boilerplate-explained-in-simple-terms/ -->
<html>
    <head>
	<!-- Meta Information -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="title" content="GABA Portal">
        <meta name="description" content="Online Portal zur gemeinsamen Betrachtung und Diagnose von DICOM Bildern.">
        <meta name="robots" content="index,nofollow">
        
        <!-- Not good here: <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <!-- Page Title -->
        <title>GABA Portal</title>
        
    <!-- FavIcons -->
    <!-- Apple -->
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
        <meta name="apple-mobile-web-app-title" content="GABA">
    <!-- Browsers -->
        <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <!-- Windows 8 -->
        <meta name="msapplication-TileColor" content="#603cba">
        <meta name="msapplication-TileImage" content="/mstile-144x144.png">
        <meta name="application-name" content="GABA">

        
    <!-- Style Sheets -->
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
                <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <!-- Load Fonts--> 
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto'  type='text/css'>

    <!-- JavaScript -->
    <!-- Note: Avoid loading javascript here: load it at the end of the page. see yahoo performance guide-->
    <!-- But: Load modernizr here to support browsers who don't know HTML5 tags -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script> 
    </head>


    
    <body>
        <!-- Screw you, IE Users-->
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="pageBackground">
             <?php
                include("header_portal.php");
                include("nav.php")
            ?>

            <!-- specific page content -->
            <h2>Aufenthalte</h2>

            <?php
                $pid =    $_SESSION['pid'];

                $verbindung = mysql_connect("localhost", "root", "")
                    or die("Verbindung zur Datenbank konnte nicht hergestellt werden");
                mysql_select_db("gaba") or die ("Datenbank konnte nicht ausgewählt werden");

                $abfrage = "SELECT AufenthaltArzt.*,Person.Titel,Person.Vorname,Person.Nachname, ICD10.Beschreibung FROM (SELECT Aufenthalt.*,behandeln.Arzt_id,behandeln.IsBehandler FROM Aufenthalt JOIN behandeln ON Aufenthalt.AID=behandeln.Aufenthalt_id WHERE isBehandler=true AND Person_id=$pid) as AufenthaltArzt JOIN Person ON AufenthaltArzt.Arzt_id=Person.PID LEFT JOIN ICD10 ON icd_code=Code ORDER BY AID desc;";

                $ergebnis = mysql_query($abfrage);

                if(mysql_num_rows($ergebnis)==0)
                {
                    echo "<p>Keine Aufenthalte.</p>";
                }

                echo "<div id=\"accordion\">";
                while ($row = mysql_fetch_object($ergebnis)) {
                    //Aufenthaltsdaten:
                    if($row->bis!=NULL)
                    {
                        echo "<h3>Aufenthalt #$row->AID <span class=\"date\" style=\"font-size:medium\"><span class=\"show\" >von</span> $row->von <span class=\"show\">bis</span> $row->bis</span></h3>";
                    }else
                    {
                        echo "<h3>Aufenthalt #$row->AID <span class=\"date\" style=\"font-size:medium\"><span class=\"show\" >von</span> $row->von</span></h3>";
                    }
                    
                    echo "<div>";
					
					echo "<p id=\"diagnose\"><span class=\"label\">ICD-Diagnose: </span> ";
					
					if($row->bis) {
						if($row->icd_code)
						{
							echo $row->icd_code;
						}else
						{
							echo " Keine. ";
						}
	                    echo "<p>";
						
						echo "<p><span class=\"label\">Beschreibung:</span> ";					
						if($row->Beschreibung)
						{
							echo $row->Beschreibung;
						}						
						echo "<p>";
						
						echo "<p><span class=\"label\">Anmerkungen:</span> ";					
						if($row->Diagnosenbeschreibung)
						{
							echo $row->Diagnosenbeschreibung;
						}						
						echo "<p>";
					} else {
						echo " Aufenthalt noch nicht abgeschlossen. ";
						echo "<p>";
					}

                    echo "<p><span class=\"label\">Behandelnder Arzt:</span> <a href=\"patient.php?pid=$row->Arzt_id\">$row->Titel $row->Vorname $row->Nachname</a></p>";
                    echo "<div class=\"clearfix\">";
                    //Aufenthaltsbilder:
                    $bilderQuery = "SELECT * FROM gaba.gehoert JOIN Bild on gehoert.Bild_id=Bild.BID where Aufenthalt_id=$row->AID AND isOriginal=1";
                     $bilder = mysql_query($bilderQuery);
                    while ($bild = mysql_fetch_object($bilder)) {
                        echo "<a href=\"bild.php?bid=$bild->BID&aid=$row->AID\"><img class=\"thumbnail\" src='$bild->Speicherort' alt=\"$bild->Beschreibung\"/></a>";
                    }
                    echo "</div></div>";
                }

                echo "</div>";
                mysql_close($verbindung);
            ?>

           
            <!-- specific page content end -->
        </div>
        <?php include("footer.html"); ?>
        <!-- JavaScript -->
        <!-- Note: Place as much of your scripts as possible here. Because: DOM is ready now, i.e. not timing issues -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Load local copy if Google CDN is down / unreachable -->
        <script>
            window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')
            var filename =  document.location.pathname.match(/[^\/]+$/)[0];
            $anchor = $('#linklist a[href="' + filename + '"]');
            $anchor.addClass("highlight");
        </script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script>
            $( "#accordion" ).accordion();
        </script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
