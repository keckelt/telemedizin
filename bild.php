<!-- Überprüfe ob session vorhanden ist, sonst Login-Aufforderung -->
<?php include('session.php'); ?>
<!-- IF POST is set, write new comment to database -->
<?php
    if (isset($_POST['text']))
    {
        $conn = new mysqli("localhost", "root", "", "gaba");

        //echo $_POST['text'];
        //echo $_POST['x_coord'];
        //echo $_POST['y_coord'];

         $now = date("Y-m-d H:i:s");

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        //Create Comment:
        $insert = "INSERT INTO `gaba`.`Kommentar` (`Inhalt`, `Datum`, `x`, `y`, `Bild_id`, `Arzt_id`) VALUES ('".$_POST['text']."', '$now','".$_POST['x_coord']."', '".$_POST['y_coord']."', '".$_GET['bid']."', '".$_SESSION['pid']."');";

        $commentID = null;
        if ($conn->query($insert) === TRUE) {
            //echo "insert comment succesful";
           $commentID = mysqli_insert_id($conn);
        } else {
            echo "Error inserting comment: " . $conn->error;
        }

        if(isset($commentID))
        {
            //Create Notification
            //Freigegebene Ärzte auslesen:
            $getDoctors = "SELECT Arzt_id FROM `behandeln` WHERE Arzt_id<>" . $_SESSION['pid'] . " AND Aufenthalt_id=". $_GET['aid'] .";";

            $result = $conn->query($getDoctors);

            if ($result->num_rows > 0) {
                // output data of each row
                while($row = $result->fetch_assoc()) {
                    //Add Notification for each doctor:
                    $insert = "INSERT INTO `gaba`.`notify` (`Kommentar_id`, `Arzt_id`) VALUES ('$commentID', '". $row["Arzt_id"] ."')";
                     if ($conn->query($insert) === TRUE) {
                        //echo "insert notification succesful";
                    } else {
                        echo "Error inserting notification: " . $conn->error ."<br>".$insert;
                    }
                }
            }
        }

        $conn->close();
	}
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<!-- Description: http://ningbit.github.io/blog/2013/09/30/html5-boilerplate-explained-in-simple-terms/ -->
<html>
<head>

    <style>
      video {
      border-radius:15px;
      margin-top:40px;
      width: 380px;
      height: 290px;
      text-align: center;
      background-color: #CCCCCC;
      float: left;
      }

    </style>

	<!-- Meta Information -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="title" content="GABA Portal">
    <meta name="description" content="Online Portal zur gemeinsamen Betrachtung und Diagnose von DICOM Bildern.">
    <meta name="robots" content="index,nofollow">

    <!-- Not good here: <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->

    <!-- Page Title -->
    <title>GABA Portal</title>

    <!-- FavIcons -->
    <!-- Apple -->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <meta name="apple-mobile-web-app-title" content="GABA">
    <!-- Browsers -->
    <link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <!-- Windows 8 -->
    <meta name="msapplication-TileColor" content="#603cba">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="application-name" content="GABA">


    <!-- Style Sheets -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel='stylesheet' href='css/spectrum.css' />
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
    <!-- Load Fonts--> 
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans|Roboto'  type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- JavaScript -->
    <!-- Note: Avoid loading javascript here: load it at the end of the page. see yahoo performance guide-->
    <!-- But: Load modernizr here to support browsers who don't know HTML5 tags -->
    <script src="js/vendor/modernizr-2.6.2.min.js"></script> 
</head>



<body>
    <!-- Screw you, IE Users-->
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
            <![endif]-->

            <div id="pageBackground">
             <?php
             include("header_portal.php");
             include("nav.php")
             ?>

             <!-- specific page content -->

             <?php
             $bid = $_GET['bid'];
             $aid = $_GET['aid'];
            
             $curURL = $_SERVER['PHP_SELF']."?bid=".$bid."&aid=".$aid;    
        
             $verbindung = mysql_connect("localhost", "root", "")
             or die("Verbindung zur Datenbank konnte nicht hergestellt werden");
             mysql_select_db("gaba") or die ("Datenbank konnte nicht ausgewählt werden");

             $abfrage = "SELECT Datum,Speicherort FROM Bild where BID=$bid;";

             $ergebnis = mysql_query($abfrage);
             $row = mysql_fetch_array($ergebnis, MYSQL_NUM);
             echo "<h2>$row[0]</h2>";

             //  temporary storage for JS variables
             echo "<div data-source='$row[1]' data-BID='$bid' data-name='$name' data-AID='$aid' id=\"data-source\"></div>";
             ?>

             <table style="width: 100%;">
                <tr>
                    <td style="width: 512px;">
                        <div style="position: relative; width: 512px;" id="sketchContainer">
                            <canvas id="bg"
                            style="position: absolute; left: 0; top: 0; z-index: 0; border:1px solid #000000;"></canvas>
                            <canvas id="wb"
                            style="position: absolute; left: 0; top: 0; z-index: 1;"></canvas>
                            <canvas id="ccan"
                            style="position: absolute; left: 0; top: 0; z-index: 2;"></canvas>
                        </div>
                    </td>
                    <td>
                        <div>
                        <table>
                            <tr>
                                <td style="display: none">
    
                                    <div id="livevideodivk" ><video id="sourcevid" muted="muted" autoplay></video></div>

                                </td>
                                <td style="display: none" id="chatWindow">

                                    <div id="remotevideodivk" ><video id="remotevid" autoplay style="margin-left:30px;"></video></div>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="display: none" id="chatButton">
    
                                <center>
                                    <br>
                                    <input id="btn" type="button" class="button" onclick="startChat();" value="Start Videochat">
                                    <br>
                                </center>

                                </td>
                            </tr>
                        </table>
                        </div>    
                    </td>
                    <!--<td id="comments">
                    <?php
                        /*
                        echo "<div id=\"c_buttons\">";
                    
                        echo "<button id=\"c_h_s\" class=\"button\">";
                        //echo "     <span class=\"fa-stack fa-lg\">";
                        //echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
                        echo "         <i class=\"fa fa-comments\"></i> Kommentare ein/aus";
                        //echo "     </span>";
                        echo "</button>";

                        echo "<button id=\"newCommentEdit\" class=\"button\">";
                        //echo "     <span class=\"fa-stack fa-lg\">";
                        //echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
                        echo "         <i class=\"fa fa-plus\"></i> neuer Kommentar";
                        //echo "     </span>";
                        echo "</button>";

                        echo "</div>";
                        
                        
                        echo "</div id=\"comment_edit\">";
                        echo "<form id=\"form_Comment\" action=\"$curURL\" method=\"post\">";
                        //echo "<table>";

                        //echo "<tr>";
                        echo "<br>";
                       
                        echo "<textarea id=\"text\" name=\"text\" maxlength=\"150\"></textarea>";
                        //echo "</tr>";
                        echo "<br>";
                        //echo "<tr>";
                        echo "<input type=\"text\" id=\"x_coord\" name=\"x_coord\" maxlength=\"3\">/<input type=\"text\" id=\"y_coord\" name=\"y_coord\" maxlength=\"3\">";
                        //echo "</tr>";
                        echo "<br>";
                        //echo "<input type=\"hidden\" id=\"bild_id\" value=\"$bid\">";
                        //echo "<input type=\"hidden\" id=\"arzt_id\" value=\"$aid\">";

                        //echo "<tr>";
                        //echo "<input name=\"cancelCommet\"  class=\"button\" id=\"cancelCommetBtn\" type=\"submit\" value=\"Abbrechen\">";
                        //echo "<input name=\"addCommet\"  class=\"button\" id=\"newCommetBtn\" type=\"submit\" value=\"OK\">";
                        //echo "<button class=\"button\" id=\"cancelCommetBtn\">Abbrechen</button>";
                        echo "<button class=\"button\" id=\"newCommetBtn\">OK</button>";
                        //echo "</tr>";                        
                        //echo "</table>";
                        echo "</form>";
                        
                        echo "</div>";

                        $abfrage = "SELECT * FROM Kommentar JOIN Person ON (Arzt_id=PID) WHERE Bild_id=$bid AND x>=0 AND y>=0  ORDER BY Datum DESC;";
                        $ergebnis = mysql_query($abfrage);

                        echo "<div id=\"accordion\" style=\"width: 100% !important;\">";
                        while ($row = mysql_fetch_object($ergebnis)) {
                            
                            $date_part = explode(" ", $row->Datum);
                          
                            
                            echo "<h3><span class=\"acc_item\">$row->Titel $row->Vorname $row->Nachname <span class=\"date\"><span class=\"show\">am</span> $date_part[0] <span class=\"show\">um</span> $date_part[1]</span></span></h3>"; 
                            echo "<div class=\"clearfix\">";
                            echo "<div style=\"width: 100% !important;\">";
                            echo "<p><i class=\"fa fa-quote-left fa-lg\"></i></p>";
                            echo "<p>$row->Inhalt</p>";
                            echo "<p><i class=\"fa fa-quote-right fa-lg\"></i></p>";
                            echo "<p>Koordinaten:</br><span class=\"coord_show\">x: </span><span id=\"c_x_c\">$row->x</span>  <span class=\"coord_show\">y: </span><span id=\"c_y_c\">$row->y</span></p>";
                            
                            echo "</div>";
                            echo "</div>";
                                                        
                        }

                        echo "</div>";*/
                        

                    ?>
                    </td>-->
                </tr>
                <tr>
                    <td id="commentPanel"  style="vertical-align: top">
                        <input type="button" class="button" id="start" value="Whiteboard starten">
                    </td>
                    <td id="sketchPanel" style="display: none; vertical-align: top">
                        <div>
                            <input type="button" class="button" id="stroke" value="Stift">
                            <input type="button" class="button" id="eraser" value="Radierer">
                            <input type='text' class="color_picker"/>
                            <p>
                            <input type="button" class="button" id="plus" value="+">
                            <ins id="bsize">00</ins>
                            <input type="button" class="button" id="minus" value="-">
                            </p>
                            <p>
                            <input type="button" class="button" id="clear" value="Löschen">
                            <input type="button" class="button" id="save" value="Speichern">
                            </p>
                            <input type="button" class="button" id="stop" value="Whiteboard stoppen" onclick="onHangUp();">
                        </div>
                    </td>
                    <td id="comments">
                    <?php

                        echo "<div id=\"c_buttons\">";
                    
                        echo "<button id=\"c_h_s\" class=\"button\">";
                        //echo "     <span class=\"fa-stack fa-lg\">";
                        //echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
                        echo "         <i class=\"fa fa-comments\"></i> Kommentare ein/aus";
                        //echo "     </span>";
                        echo "</button>";

                        echo "<button id=\"newCommentEdit\" class=\"button\">";
                        //echo "     <span class=\"fa-stack fa-lg\">";
                        //echo "         <i class=\"fa fa-circle fa-stack-2x\"></i>";
                        echo "         <i class=\"fa fa-plus\"></i> neuer Kommentar";
                        //echo "     </span>";
                        echo "</button>";

                        echo "</div>";
                        
                        
                        echo "</div id=\"comment_edit\">";
                        echo "<form id=\"form_Comment\" action=\"$curURL\" method=\"post\">";
                        //echo "<table>";

                        //echo "<tr>";
                        echo "<br>";
                       
                        echo "<textarea id=\"text\" name=\"text\" maxlength=\"150\"></textarea>";
                        //echo "</tr>";
                        echo "<br>";
                        //echo "<tr>";
                        echo "<input type=\"text\" id=\"x_coord\" name=\"x_coord\" maxlength=\"3\">/<input type=\"text\" id=\"y_coord\" name=\"y_coord\" maxlength=\"3\">";
                        //echo "</tr>";
                        echo "<br>";
                        //echo "<input type=\"hidden\" id=\"bild_id\" value=\"$bid\">";
                        //echo "<input type=\"hidden\" id=\"arzt_id\" value=\"$aid\">";

                        //echo "<tr>";
                        //echo "<input name=\"cancelCommet\"  class=\"button\" id=\"cancelCommetBtn\" type=\"submit\" value=\"Abbrechen\">";
                        //echo "<input name=\"addCommet\"  class=\"button\" id=\"newCommetBtn\" type=\"submit\" value=\"OK\">";
                        //echo "<button class=\"button\" id=\"cancelCommetBtn\">Abbrechen</button>";
                        echo "<button class=\"button\" id=\"newCommetBtn\">OK</button>";
                        //echo "</tr>";                        
                        //echo "</table>";
                        echo "</form>";
                        
                        echo "</div>";

                        $abfrage = "SELECT * FROM Kommentar JOIN Person ON (Arzt_id=PID) WHERE Bild_id=$bid AND x>=0 AND y>=0  ORDER BY Datum DESC;";
                        $ergebnis = mysql_query($abfrage);

                        echo "<div id=\"accordion\" style=\"width: 100% !important;\">";
                        while ($row = mysql_fetch_object($ergebnis)) {
                            
                            $date_part = explode(" ", $row->Datum);
                          
                            
                            echo "<h3><span class=\"acc_item\">$row->Titel $row->Vorname $row->Nachname <span class=\"date\"><span class=\"show\">am</span> $date_part[0] <span class=\"show\">um</span> $date_part[1]</span></span></h3>"; 
                            echo "<div class=\"clearfix\">";
                            echo "<div style=\"width: 100% !important;\">";
                            echo "<p><i class=\"fa fa-quote-left fa-lg\"></i></p>";
                            echo "<p>$row->Inhalt</p>";
                            echo "<p><i class=\"fa fa-quote-right fa-lg\"></i></p>";
                            echo "<p>Koordinaten:</br><span class=\"coord_show\">x: </span><span id=\"c_x_c\">$row->x</span>  <span class=\"coord_show\">y: </span><span id=\"c_y_c\">$row->y</span></p>";
                            
                            echo "</div>";
                            echo "</div>";
                                                        
                        }

                        echo "</div>";
                        

                    ?>
                    </td>
                    <!--<td>

                        <table>
                            <tr>
                                <td style="display: none">
    
                                    <div id="livevideodivk" ><video id="sourcevid" muted="muted" autoplay></video></div>

                                </td>
                                <td style="display: none" id="chatWindow">

                                    <div id="remotevideodivk" ><video id="remotevid" autoplay style="margin-left:30px;"></video></div>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="display: none" id="chatButton">
    
                                <center>
                                    <br>
                                    <input id="btn" type="button" class="button" onclick="connect();" value="Start Videochat">
                                    <br>
                                </center>

                                </td>
                            </tr>
                        </table>

                    </td>-->
                </tr>
            </table>
                
            <hr>
            
            <?php
                //Bilder einlesen die Teil des Bildes sind:
                $abfrage = "SELECT * FROM Bild WHERE BID=$bid;";
                $ergebnis = mysql_query($abfrage);
                echo "<div class=\"clearfix\" id=\"sketchImg\">";

                while($original = mysql_fetch_object($ergebnis))
                {
                    echo "<img id=\"$original->BID\" alt=\"original\" class=\"thumbnail\" src=\"$original->Speicherort\">";
                }

                $abfrage = "SELECT * FROM Bild WHERE teil_von=$bid;";
                $ergebnis = mysql_query($abfrage);

                while($teilbild = mysql_fetch_object($ergebnis))
                {
                    echo "<img id=\"$teilbild->BID\" class=\"thumbnail\" src=\"$teilbild->Speicherort\">";
                }
                echo "</div>";

            ?>

        </div>
             <!-- specific page content end -->
         <?php include("footer.html"); ?>
         <!-- JavaScript -->
         <!-- Note: Place as much of your scripts as possible here. Because: DOM is ready now, i.e. not timing issues -->
         <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
         <!-- Load local copy if Google CDN is down / unreachable -->
         <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
         <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        
         <script src="js/plugins.js"></script>
         <script src="js/main.js"></script>

         <script>

            // togetherJS configuration
            var TogetherJSConfig_suppressInvite = true;
            var TogetherJSConfig_suppressJoinConfirmation = true;
            var TogetherJSConfig_getUserName = document.getElementById("data-source").getAttribute("data-name");
            var TogetherJSConfig_disableWebRTC = true;

        </script>
            
       <script>
            //window.addEventListener('DOMContentLoaded', function (){     
            window.addEventListener("load", function (){   
                var c_canvas = document.getElementById('ccan');
                
                $("#ccan").show();
                $("#accordion").show();
                
                
                $("#form_Comment").hide();
                
                
                document.getElementById("c_h_s").addEventListener("click", function (){
                    if($("#accordion").is(":visible")) 
                    {
                        //$("#accordion").hide();
                        //$("#ccan").hide();
                        
                        $("#accordion").fadeOut("slow");
                        $("#ccan").fadeOut("slow");
                        
                    }else
                    {
                        $("#accordion").fadeIn("slow");
                        $("#ccan").fadeIn("slow");
                        
                        //$("#accordion").show();
                        //$("#ccan").show();
                    }
                    
                    
                });
                
                
                document.getElementById("newCommentEdit").addEventListener("click", function (){
                    if($("#form_Comment").is(":visible")) 
                    {
                        //$("#form_Comment").hide();
                        $("#form_Comment").fadeOut("slow");
                        
                        if(!$("#accordion").is(":visible"))
                        {
                            //$("#ccan").hide();
                            $("#ccan").fadeOut("slow");
                        }
                        
                    }else
                    {
                        //$("#form_Comment").show();
                        //$("#ccan").show();
                        
                        $("#form_Comment").fadeIn("slow");
                        $("#ccan").fadeIn("slow");
                        
                        clearForm();
                    }   
                    
                });
                
                
                function clearForm()
                {
                    var text = document.getElementById("text");
                    var x_coord = document.getElementById("x_coord");
                    var y_coord = document.getElementById("y_coord");
                    
                    text.value = "";
                    x_coord.value = "";
                    y_coord.value = "";
                    
                }
                
                
                document.getElementById("newCommetBtn").addEventListener("click", function (){
                        // maybe not needed 
                });
                
                
                c_canvas.addEventListener('mousedown', function (){
                    var x = new Number();
                    var y = new Number();
                    var canvas = document.getElementById('ccan');
                    var x_coord = document.getElementById('x_coord');
                    var y_coord = document.getElementById('y_coord');

                    var currentElement = canvas;
                    var totalOffsetX = 0;
                    var totalOffsetY = 0;
                    
                    if (event.pageX != undefined && event.pageY != undefined)
                    {
                        x = event.pageX;
                        y = event.pageY;
                    }
                    else // Firefox method to get the position --> does not work
                    {
                        x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                        y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
                    }
                    
                    if (canvas.offsetLeft != undefined && canvas.offsetTop != undefined)
                    {
                        x -= canvas.offsetLeft;
                        y -= canvas.offsetTop;
                    }
                    
                    //berechnen des offsets über alle parents
                    do{
                        totalOffsetX += currentElement.offsetLeft - currentElement.scrollLeft;
                        totalOffsetY += currentElement.offsetTop - currentElement.scrollTop;
                    }
                    while(currentElement = currentElement.offsetParent)
                    
                        
                        x -= totalOffsetX;
                        y -= totalOffsetY;
                    
                    
                        x_coord.value = x;
                        y_coord.value = y;
            
                              
                });
                      
                
            
            });
        
           function drawMark(x,y)
            {
                $("#ccan").show();
                var c = document.getElementById("ccan");
                var ctx = c.getContext("2d");
                
                ctx.clearRect ( 0 , 0 , c.width, c.height );
               
                if(x>0 && y>0)
                {
                    var b_size1 = 24;
                    var pos_offset = b_size1/2;
                    ctx.beginPath();
                    ctx.lineWidth = "4";
                    ctx.fillStyle = "#72E38F";
                    ctx.rect(x-(b_size1/2),y-(b_size1/2),b_size1,b_size1);
                    ctx.fill();

                    var b_size2 = 16;
                    ctx.beginPath();
                    ctx.lineWidth = "4";
                    ctx.fillStyle = "white";
                    ctx.rect(x-(b_size2/2),y-(b_size2/2),b_size2,b_size2);
                    ctx.fill();

                    var b_size3 = 8;
                    ctx.beginPath();
                    ctx.lineWidth = "4";
                    ctx.fillStyle = "#72E38F";
                    ctx.rect(x-(b_size3/2),y-(b_size3/2),b_size3,b_size3);
                    ctx.fill();
                }
            }
             
            $( "#accordion" ).on( "accordionbeforeactivate", function( event, ui ) {
                var x_acc = ui.newPanel.find("#c_x_c").text();
                var y_acc = ui.newPanel.find("#c_y_c").text();

                console.log(x_acc);
                console.log(y_acc);

                drawMark(x_acc,y_acc);
            } );
            
            $( "#accordion" ).accordion({
              active: false,
              collapsible: true
            });
         </script>

        <script src="https://togetherjs.com/togetherjs-min.js"></script>

        <script src="http://code.jquery.com/jquery-latest.js"></script> <!-- brauch ma des ? -->

        <script src='js/spectrum.js'></script>

        <script src="js/sketch.js"></script>

        <script src="js/video.js"></script>
    </body>
</html>
