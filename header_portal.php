<header>
    <div class="wrapper">
        <div id="header-left">
            <a href="portal-home.php"> <img src="img/logo-128px.png" alt="Logo"/> </a>
        </div>
        <div id="header-right">
            <h1>GABA</h1>
            <p>Gemeinsam Aufnahmen Bearbeiten und Ansehen.</p>
        </div>

        <div id="userinfo">
            <?php
                echo "Benutzer: ";
                echo $_SESSION['name'];
                $name = $_SESSION['name'];
            ?>
            <br>
            <br>
            <form name="input" action="logout.php" method="post">
                <input class="button" id="logoffBtn" type="submit" value="Abmelden">
            </form>
        </div>

    </div>
</header>