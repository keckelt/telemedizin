<?php
    session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
    <head>
		<!-- Meta Information -->
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="title" content="GABA Portal">
		<meta name="description" content="Online Portal zur gemeinsamen Betrachtung und Diagnose von DICOM Bildern.">
		<meta name="robots" content="index,nofollow">

		<!-- Page Title -->
		<title>Registrierung</title>
			
		<!-- FavIcons -->
		<!-- Apple -->
		<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
		<meta name="apple-mobile-web-app-title" content="GABA">
		<!-- Browsers -->
		<link rel="icon" type="image/png" href="/favicon-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
		<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<!-- Windows 8 -->
		<meta name="msapplication-TileColor" content="#603cba">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="application-name" content="GABA">

			
		<!-- Style Sheets -->
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/main.css">
		<!-- Load Fonts--> 
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans|Roboto"  type="text/css">

		<!-- JavaScript -->
		<!-- Note: Avoid loading javascript here: load it at the end of the page. see yahoo performance guide-->
		<!-- But: Load modernizr here to support browsers who don"t know HTML5 tags -->
		<script src="js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="js/gen_validatorv4.js" type="text/javascript"></script>		
    </head>
	
	<body>
	    <div id="pageBackground">        
            <?php include("header.html");

                if(!isset($_GET['patient']))
                {
                    echo "<nav> <ul> <li>";
				    echo "<a href=\"index.php\">Home</a>";
                    echo "</li> </ul> </nav>";
                }else{
                    include("nav.php");
                }
            ?>
			
			<h4> <i> * = ben&ouml;tigte Felder </i> </h4>
			
			<form id="register" action="registerToDB.php" method="post" accept-charset="UTF-8">
				<fieldset>
					<div style="width:33%;float:left">
						<label for="title" >Titel </label> <br />
						<input type="text" name="title" id="title"/>
						<span id="register_title_errorloc" style="color:red;"></span> <br />

						<label for="lastname" >Nachname* </label> <br />
						<input type="text" name="lastname" id="lastname"/>
						<span id="register_lastname_errorloc" style="color:red;"></span> <br />
					
						<label for="firstname" >Vorname* </label> <br />
						<input type="text" name="firstname" id="firstname"/>
						<span id="register_firstname_errorloc" style="color:red;"></span> <br />
						
						<label for="email" >Email*</label> <br />
						<input type="text" name="email" id="email"/>
						<span id="register_email_errorloc" style="color:red;"></span> <br />
					
						<label for="username" >Benutzername*</label> <br />
						<input type="text" name="username" id="username"/>
						<span id="register_username_errorloc" style="color:red;"></span> <br />
					
						<label for="password" >Passwort*</label> <br />
						<input type="password" name="password" id="password"/>
						<span id="register_password_errorloc" style="color:red;"></span> <br />
					</div>
				
					<div style="width:33%;float:left">
						<label for="sex" >Geschlecht*</label> <br />
						<input type="radio" name="sex" value="m" checked> m&auml;nnlich &nbsp; </input>
						<input type="radio" name="sex" value="w"> weiblich </input> <br />
					
                    <?php
                        if(!isset($_GET['patient']))
                        {
                            echo "<label for=\"doctor\">Sind Sie Arzt?*</label> <br />";
                            echo "<input type=\"checkbox\" name=\"doctor\" value=\"true\"> Ja </input> <br />";                          
                        }else{
                            echo "<input type=\"hidden\" name=\"doctor\" value=\"false\"></input>";
                            echo "<input type=\"hidden\" name=\"back2portal\" value=\"back2portal\"></input>";
                            echo "<input type=\"checkbox\" name=\"dummy\" value=\"true\" style=\"visibility: hidden;\" /> <br /> <br />";
                        }

                    ?>
										
						<label for="birthday" >Geburtstag*</label> <br />
						<input type="date" name="birthday" id="birthday"/>
						<span id="register_birthday_errorloc" style="color:red;"></span> <br />
					
						<label for="svnr" >SVNR*</label> <br />
						<input type="text" name="svnr" id="svnr"/>
						<span id="register_svnr_errorloc" style="color:red;"></span> <br />
					
						<label for="phone" >Telefon</label> <br />
						<input type="text" name="phone" id="phone"/>
						<span id="register_phone_errorloc" style="color:red;"></span> <br /> <br />
						
						<input class="button" id="registerBtn" name="submit" type="submit" value="Absenden" style="width:43%">						
					</div>
				</fieldset>
			</form>
			
			<!-- Validation Javascript -->
			<script  type="text/javascript">
				var frmvalidator = new Validator("register");
				frmvalidator.EnableOnPageErrorDisplay();
				frmvalidator.EnableMsgsTogether();
				
				frmvalidator.addValidation("title","alpha_s","Kein korrekter Titel");
				frmvalidator.addValidation("title","maxlen=10","Kein korrekter Titel");
				
				frmvalidator.addValidation("lastname","alpha","Kein korrekter Nachname");
				frmvalidator.addValidation("lastname","maxlen=20","Kein korrekter Nachname");
				frmvalidator.addValidation("lastname","minlen=3","Kein korrekter Nachname");
				frmvalidator.addValidation("lastname","req","Nachnamen bitte angeben");
				
				frmvalidator.addValidation("firstname","alpha","Kein korrekter Vorname");
				frmvalidator.addValidation("firstname","maxlen=20","Kein korrekter Vorname");
				frmvalidator.addValidation("firstname","minlen=3","Kein korrekter Vorname");
				frmvalidator.addValidation("firstname","req","Vornamen bitte angeben");
				
				frmvalidator.addValidation("email","email","Keine korrekte Email");
				frmvalidator.addValidation("email","req","Bitte Email-Adresse angeben");
				
				frmvalidator.addValidation("username","alphanumeric","Kein korrekter Benutzername");
				frmvalidator.addValidation("username","maxlen=10","Benutzername zu lang");
				frmvalidator.addValidation("username","minlen=3","Benutzername zu kurz");
				frmvalidator.addValidation("username","req","Bitte Benutzernamen angeben");
				
				frmvalidator.addValidation("password","minlen=3","Passwort zu kurz");
				frmvalidator.addValidation("password","req","Bitte Passwort angeben");
				
				frmvalidator.addValidation("birthday","regexp=[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]","Format: JJJJ-MM-TT");
				frmvalidator.addValidation("birthday","req","Geburtstag angeben");
				frmvalidator.addValidation("birthday","minlen=10","Format: JJJJ-MM-TT");
				frmvalidator.addValidation("birthday","maxlen=10","Format: JJJJ-MM-TT");
				
				frmvalidator.addValidation("svnr","numeric","SVNR ist eine Zahl mit 4 Stellen");
				frmvalidator.addValidation("svnr","req","SVNR bitte angeben");
				frmvalidator.addValidation("svnr","minlen=4","SVNR ist eine 4-stellige Zahl");
				frmvalidator.addValidation("svnr","maxlen=4","SVNR ist eine 4-stellige Zahl");
				
				frmvalidator.addValidation("phone","numeric","Keine korrekte Telefonnummer");				
			</script>
			
		</div>
        <?php include("footer.html"); ?>

        <!-- JavaScript -->
        <!-- Note: Place as much of your scripts as possible here. Because: DOM is ready now, i.e. not timing issues -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Load local copy if Google CDN is down / unreachable -->
        <script>window.jQuery || document.write("<script src="js/vendor/jquery-1.11.1.min.js"><\/script>")</script>
        <script src="https://togetherjs.com/togetherjs-min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
	</body>
</html>