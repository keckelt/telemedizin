<?php
	$mysql_host     = "localhost";
	$mysql_username = "root";
	$mysql_password = "";
	$mysql_database = "gaba";

	//Write into person
	$mysqli  = new Mysqli($mysql_host, $mysql_username, $mysql_password, $mysql_database);
	$prepare = $mysqli->prepare("INSERT INTO Person (Titel,Vorname,Nachname,Email,Geburtsdatum,SVNR,Tel,Geschlecht,isArzt) VALUES (?,?,?,?,?,?,?,?,?)");
	$isdoctor = ($_POST['doctor'] == 'true');
	//Check checkbox
	if(isset($_POST['doctor']) && $_POST['doctor'] == 'true')
	{
		$isdoctor = 1;
	}
	else
	{
		$isdoctor = 0;
	}
	$prepare->bind_param("ssssssssi", $_POST['title'], $_POST['firstname'], $_POST['lastname'], $_POST['email'],       $_POST['birthday'], $_POST['svnr'], $_POST['phone'], $_POST['sex'], $isdoctor);

    $error = false;
    if (!$prepare->execute()) {
        echo "Execute failed: (" . $prepare->errno . ") " . $prepare->error;
        $error = true;
    }

	//Write into login, subquery for highest(last) person id
	$preLogin = $mysqli->prepare("INSERT INTO Login (Person_id,Benutzername,Passwort) VALUES ((select MAX(PID) PID from Person),?,?)");
	$preLogin->bind_param("ss", $_POST['username'], $_POST['password']);

    if (!$preLogin->execute()) {
        echo "Execute failed: (" . $preLogin->errno . ") " . $preLogin->error;
        $error = true;
    }
	$mysqli->close();

	//Link to thank you for registering page
    if(!isset($_POST['back2portal'])){
        if(!$error)
        {
            header('Location: index.php?msg=1');
        } else {
            header('Location: index.php?msg=0');
        }
    }
    else{
        if(!$error)
        {
            header('Location: patienten.php');
        }
    }
?>