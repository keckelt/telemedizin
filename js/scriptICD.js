// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#icdcode').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'searchICD.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#listICD').show();
				$('#listICD').html(data);
			}
		});
	} else {
		$('#listICD').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#icdcode').val(item);
	// hide proposition list
	$('#listICD').hide();
}