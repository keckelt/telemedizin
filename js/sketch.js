 window.onload = function() {

  // get the canvas element and its context
  var canvas = document.getElementById('wb');
  var context = canvas.getContext('2d');

  var bgcanvas = document.getElementById('bg');
  var bgcontext = bg.getContext('2d');
     
  var c_canvas = document.getElementById('ccan');
  var c_context = bg.getContext('2d');

  var imageObj = new Image();

  var ratio = 1;

  var descr = "Erstellt von: ";

  // set background and canvas size
  imageObj.onload = function() {
    ratio = imageObj.width / imageObj.height;

    // set height and width of the whiteboard, background and div container
    $('#sketchContainer').width(512);
    $('#sketchContainer').height(512*ratio);

    canvas.width = $('#sketchContainer').width();
    canvas.height = $('#sketchContainer').height();

    bgcanvas.width = $('#sketchContainer').width();
    bgcanvas.height = $('#sketchContainer').height();

    c_canvas.width = $('#sketchContainer').width();
    c_canvas.height = $('#sketchContainer').height();  
      
    // scale function needs to know the width/height pre-resizing:
    oWidth = canvas.width;
    oHeight = canvas.height;
    lines = [];

    //draw background
    bgcontext.drawImage(imageObj, 0, 0,512,512*ratio);

  };
  imageObj.src = document.getElementById("data-source").getAttribute("data-source");

  lastMouse = {
    x: 0,
    y: 0
  };

  // brush settings
  context.lineWidth = 2;
  context.lineJoin = 'round';
  context.lineCap = 'round';
  context.strokeStyle = '#000000';

  document.getElementById("bsize").innerHTML = "0" + context.lineWidth;

  // attach the mousedown, mouseout, mousemove, mouseup event listeners.
  function startWB() {

    getTogether();

    $("#commentPanel").fadeOut("fast", function(){
      $("#accordion").fadeOut("slow"),
      $("#ccan").fadeOut("sloq"),
      $("#sketchPanel").fadeIn("slow"),
      $("#chatWindow").fadeIn("slow"),
      $("#chatButton").fadeIn("slow")
    });

  canvas.addEventListener('mousedown', onMouseDown, false);

  canvas.addEventListener('mouseout', function () {
    canvas.removeEventListener('mousemove', move, false);
  }, false);

  canvas.addEventListener('mouseup', function () {
    canvas.removeEventListener('mousemove', move, false);
  }, false);

  }

  function stopWB() {
    canvas.removeEventListener('mousedown', onMouseDown, false);
    setTogether(0);

  }

  function getTogether() {
    var bid = document.getElementById("data-source").getAttribute("data-BID");
    $.post("getTogetherJS.php", {
      BID : bid
    },
    function(data,status){
    var use = data.substring(0, 1);
    var url = data.substring(1);
    if(use == 1){
      window.location = url;
    }
    else{
      setTogether(1);
      updateDescr(document.getElementById("data-source").getAttribute("data-name"));
    }
    }
    );
  }

  function setTogether(use) {
    var jsURL = TogetherJS.shareUrl();
    var bid = document.getElementById("data-source").getAttribute("data-BID");
    $.post("setTogetherJS.php", {
      BID : bid,
      URL : jsURL,
      Use : use
    });
  }

  // mousedown function
  function onMouseDown(e) {
    lastMouse = {
      x: e.pageX - $('#wb').offset().left,
      y: e.pageY - $('#wb').offset().top
    };
    canvas.addEventListener('mousemove', move, false);
  }

  // Sets the brush size:
  function setSize(size) {
    if(size<10){
      document.getElementById("bsize").innerHTML = "0" + size;
    } else{
      document.getElementById("bsize").innerHTML = size;
    }
    context.lineWidth = size;
  }

  // Sets the brush color:
  function setColor(color) {
    context.globalCompositeOperation = 'source-over';
    context.strokeStyle = color;
  }

  // Sets the brush to erase-mode:
  function eraser() {
    context.globalCompositeOperation = 'destination-out';
    context.strokeStyle = 'rgba(0,0,0,1)';
  }

  // Clears the canvas and the lines-array:
  function clear(send) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    lines = [];
    if (send && TogetherJS.running) {
      TogetherJS.send({
        type: 'clear'
      });
    }
  }

  function loadSketch(src, callback){
    loadimageObj = new Image();
    loadimageObj.onload = callback;
    loadimageObj.src = src;
  }

  // Redraws the lines from the lines-array:
  function reDraw(lines){
    for (var i in lines) {
      draw(lines[i][0], lines[i][1], lines[i][2], lines[i][3], lines[i][4], false);
    }
  }
  // Draws the lines, called by move and the TogetherJS event listener:
  function draw(start, end, color, size, compositeOperation, save) {
    context.save();
    context.lineJoin = 'round'; 
    context.lineCap = 'round';
    // Since the coordinates have been translated to an 1140x400 canvas, the context needs to be scaled before it can be drawn on:
    context.scale(canvas.width/512,canvas.height/(512/ratio));
    context.strokeStyle = color;
    context.globalCompositeOperation = compositeOperation;
    context.lineWidth = size;
    context.beginPath();
    context.moveTo(start.x, start.y);
    context.lineTo(end.x, end.y);
    context.closePath();
    context.stroke();
    context.restore();
    if (save) {
    // Won't save if draw() is called from reDraw().
    lines.push([{x: start.x, y: start.y}, {x: end.x, y: end.y}, color, size, compositeOperation]);
  }
}

  // Called whenever the mousemove event is fired, calls the draw function:
  function move(e) {
    var mouse = {
      x: e.pageX - $('#wb').offset().left,
      y: e.pageY - $('#wb').offset().top
    };
    // Translates the coordinates from the local canvas size to 1140x400:
    sendMouse = {
      x: (512/canvas.width)*mouse.x,
      y: ((512/ratio)/canvas.height)*mouse.y
    };
    sendLastMouse = {
      x: (512/canvas.width)*lastMouse.x,
      y: ((512/ratio)/canvas.height)*lastMouse.y
    };
    draw(sendLastMouse, sendMouse, context.strokeStyle, context.lineWidth, context.globalCompositeOperation, true);
    if (TogetherJS.running) {
      TogetherJS.send({
        type: 'draw',
        start: sendLastMouse,
        end: sendMouse,
        color: context.strokeStyle,
        size: context.lineWidth,
        compositeOperation: context.globalCompositeOperation
      });
    }
    lastMouse = mouse;
  }

  function updateDescr(uname) {
    descr = descr + ", " + uname;
  }

  function preUpdateDescr(uname){
    updateDescr(uname);
    if (TogetherJS.running) {
      TogetherJS.send({
        type: 'updateDescr',
        uname: uname
      });
    }
  }

  TogetherJS.hub.on('updateDescr', function (msg) {
    if (!msg.sameUrl) {
      return;
    }
    updateDescr(msg.uname);
    alert(descr);
  });

  // Listens for draw messages, sends info about the drawn lines:
  TogetherJS.hub.on('draw', function (msg) {
    if (!msg.sameUrl) {
      return;
    }
    draw(msg.start, msg.end, msg.color, msg.size, msg.compositeOperation, true);
  });


  // Clears the canvas whenever someone presses the clear-button
  TogetherJS.hub.on('clear', function (msg) {
    if (!msg.sameUrl) {
      return;
    }
    clear(false);
  });

  // Hello is sent from every newly connected user, this way they will receive what has already been drawn:
  TogetherJS.hub.on('togetherjs.hello', function () {
    TogetherJS.send({
      type: 'init',
      lines: lines
    });
  });

  // Draw initially received drawings:
  TogetherJS.hub.on('init', function (msg) {
    reDraw(msg.lines);
    lines = msg.lines;
  });

  TogetherJS.hub.on('init', function () {
    startWB();
    preUpdateDescr(TogetherJS.require("peers").Self.name);
  });

  // JQuery to handle buttons and resizing events, also changes the cursor to a dot resembling the brush size:
  $(document).ready(function () {

  // changeMouse creates a temporary invisible canvas that shows the cursor, which is then set as the cursor through css:
  function changeMouse() {
    // Makes sure the cursorSize is scaled:
    var cursorSize = context.lineWidth*(canvas.width/512); 
    if (cursorSize < 10){
      cursorSize = 10;
    }
    var cursorColor = context.strokeStyle;
    var cursorGenerator = document.createElement('canvas');
    cursorGenerator.width = cursorSize;
    cursorGenerator.height = cursorSize;
    var ctx = cursorGenerator.getContext('2d');

    var centerX = cursorGenerator.width/2;
    var centerY = cursorGenerator.height/2;

    ctx.beginPath();
    ctx.arc(centerX, centerY, (cursorSize/2)-4, 0, 2 * Math.PI, false);
    ctx.lineWidth = 3;
    ctx.strokeStyle = cursorColor;
    ctx.stroke();
    $('#wb').css('cursor', 'url(' + cursorGenerator.toDataURL('image/png') + ') ' + cursorSize/2 + ' ' + cursorSize/2 + ',crosshair');
  }
  // Init mouse
  changeMouse();

  // Redraws the lines whenever the canvas is resized:
  $(window).resize(function() {
    if ($('#sketchContainer').width() != oWidth) {
      canvas.width = $('#sketchContainer').width();
      canvas.height = (canvas.width/512)*ratio;
      $('#sketchContainer').outerHeight(String(canvas.height)+"px", true);
      var ratio = canvas.width/oWidth;
      oWidth = canvas.width;
      oHeight = canvas.height;
      reDraw(lines);
      changeMouse();
    }
  });
 
  // Clears the canvas:
  $('#clear').click(function () {
    if(window.confirm("Clear all?")){
      clear(true);
    }
  });

  // change to eraser
  $('#eraser').click(function () {
    eraser();
    changeMouse();
  });
  // change to stroke
  $('#stroke').click(function() {
    setColor($(".color_picker").spectrum("get").toHexString());
    changeMouse();
  });

  // Increase/decrease brush size:
  $('#plus').click(function() {
    setSize(context.lineWidth+3);
    changeMouse();
  });

  $('#minus').click(function() {
    if (context.lineWidth > 3) {
      setSize(context.lineWidth-3);
    }
    changeMouse();
  }); 

  $('#save').click(function() {
    var dataURL = canvas.toDataURL();
    var bid = document.getElementById("data-source").getAttribute("data-BID");
    var aid = document.getElementById("data-source").getAttribute("data-AID");
    $.post("process.php", {
      imageData : dataURL,
      parentID : bid,
      stayID : aid,
      descr : descr
    });
  });

  $(".color_picker").spectrum({
    color: "#000000",
    change: function(color) {
        setColor(color.toHexString());
        changeMouse();
    }
  });

  $('#start').click(function(){
    TogetherJS();
  });

  $('#stop').click(function(){
    $("#sketchPanel").fadeOut("fast", function(){
      $("#chatWindow").fadeOut("fast"),
      $("#chatButton").fadeOut("fast"),
      $("#commentPanel").fadeIn("slow")
      stopWB();
      TogetherJS();
    });
  });

  $('#sketchImg').on('click', 'img', function() {
    clear();
    if($(this).prop('alt')!="original")
    {
        loadSketch($(this).prop('src') , function() {
            context.drawImage(loadimageObj, 0, 0);
        });
    }
  });

});

  TogetherJSConfig_on = {
  ready: startWB
  };

};