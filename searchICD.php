<?php
	// Credentials
	$dbhost = "localhost";
	$dbname = "gaba";
	$dbuser = "root";
	$dbpass = "";

	//	Connection
	$tutorial_db = new mysqli();
	$tutorial_db->connect($dbhost, $dbuser, $dbpass, $dbname);
	$tutorial_db->set_charset("utf8");

	//	Check Connection
	if ($tutorial_db->connect_errno) {
		printf("Connect failed: %s\n", $tutorial_db->connect_error);
		exit();
	}

	//Get Search-String
	$keyword = '%'.$_POST['keyword'].'%';

	// Build Query
	$query = 'SELECT Code, Beschreibung FROM ICD10 WHERE Code LIKE "%'.$keyword.'%" OR Beschreibung LIKE "%'.$keyword.'%" LIMIT 15';

	// Do Search
	$result = $tutorial_db->query($query);
	while($results = $result->fetch_array()) {
		$result_array[] = $results;
	}

	// Check If We Have Results
	if (isset($result_array)) {
		foreach ($result_array as $result) {
			// put in bold the written text
			$code = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $result['Code'].': '.$result['Beschreibung']);
			// add new option
			echo '<li onclick="set_item(\''.str_replace("'", "\'", $result['Code'].': '.$result['Beschreibung']).'\')">'.$code.'</li>';
		}
	}else{
		// Output
		echo 'Keine Ergebnisse';
	}
?>